/**
 * utilities.js - useful functions for the webpage
 * @author Israel Aristide, Samir Abo-Assfour
 * @version 1.0.0
 */

"use strict"

/**
 * enable_disable_Button - A toggle function that changes the state of a button 
 * from disabled/enabled to enabled/disabled.
 * @author Samir Abo-Assfour
 */
function toggle_button(buttonId) {
    let button = document.querySelector(`button#${buttonId}`);
    if (button.disabled) {
        button.disabled = false;
    } else {
        button.disabled = true;
    }
}

/**
 * Sets if a button is enabled or disabled.
 * @param {*} buttonID 
 * @param {*} enabled 
 * @author Israel Aristide
 */
function setButtonDisabled(buttonID, disabled) {
    let button = document.querySelector(`button#${buttonID}`);
    button.disabled = disabled;
}

/**
 * set_elem_required- A function that given an element id, it makes the element required
 * and makes the font-weight of its label bold.
 * @author Samir Abo-Assfour
 */
function set_elem_required(elem_id) {

    let elem = document.getElementById(elem_id);
    elem.required = true;
    let boldLabel = document.getElementsByTagName('label');

    for (let k of boldLabel) {
        if (k.htmlFor == elem_id) {
            k.style.fontWeight = 'bold';
        }
    }
}

/**
 * createTagElement - Given a tag_object (as an object) , and a parent (as HTMLElement), 
 * it creates the tag element and append it to the parent. The tag object must have at least tagName as a key. 
 * Optionally, it can have the following keys: id, value, textContent, innerHTML.
 * @author Israel Aristide, Samir Abo-Assfour
 */
function createTagElement(tagObj, parentElement) {

    let childElement = document.createElement(tagObj.tagName);

    if ('id' in tagObj)
        childElement.id = tagObj.id;

    if ('value' in tagObj)
        childElement.value = tagObj.value;

    if ('textContent' in tagObj)
        childElement.textContent = tagObj.textContent;

    if ('innerHTML' in tagObj)
        childElement.innerHTML = tagObj.innerHTML;

    if ('class' in tagObj)
        childElement.classList.add(tagObj.class)

    parentElement.appendChild(childElement);

    return childElement;
}

/**
 * setElementFeedback -Given elem_id to identify the field, a feedback_text, and error_success, 
 * add a feedback and the error image next to the element, 
 * otherwise just the green valid image (without text)
 * @author Israel Aristide, Samir Abo-Assfour
 */

function setElementFeedback(elemid, isValid, feedbackText) {

    let elem = document.getElementById(elemid);
    let error_img = document.createElement('img');
    let valid_img = document.createElement('img');

    error_img.src = '../images/invalid.png';
    valid_img.src = '../images/valid.png';

    if (isValid) {

        elem.parentElement.children[elem.parentElement.children.length - 1].remove();
        elem.parentElement.appendChild(valid_img);
        elem.parentElement.nextElementSibling.classList.remove("invalid");
        elem.parentElement.nextElementSibling.classList.add("valid");

    } else {

        elem.parentElement.children[elem.parentElement.children.length - 1].remove();
        elem.parentElement.appendChild(error_img);
        elem.parentElement.nextElementSibling.classList.remove("valid");
        elem.parentElement.nextElementSibling.classList.add("invalid");
        elem.parentElement.nextElementSibling.textContent = feedbackText;

    }


}

/**
 * Use the right pattern to check the element for which the event (blur) occur.
 *  
 * Set the feedback accordingly and enable or disable the add button
 * @author Samir Abo-Assfour
 */

function validateElement(regexExp, elem, feedbackTxt) {
    let regex = new RegExp(regexExp);
    let verification = regex.test(elem.value);
    if (verification) {
        setElementFeedback(elem.id, verification, feedbackTxt);
    } else {
        setElementFeedback(elem.id, verification, feedbackTxt);
    }
    return verification;
}

/**
 * Takes in an object of booleans and returns true if all of them are true.
 * @param {*} verification_status 
 * @author Israel Aristide
 */
function allValid(verification_status) {

    let valid = true;

    for (let key in verification_status)
        if (!verification_status[key])
            valid = false;

    return valid
}

/**
 * createTableFromArrayObjects -Given an array of (objects) and parent element, creates the body of a table
 * attach it to parent element and populate the table with array of objects. Each object occupies a row. 
 * @author Israel Aristide, Samir Abo-Assfour
 */
function createTableContentFromArrayObjects(arrObj, parentElem) {

    for (let item of arrObj) {

        var tr = createTagElement({ tagName: "tr", class: `${parentElem.id}-tr-${arrObj.indexOf(item)}` }, parentElem);

        for (let key in item)
            createTagElement({ tagName: "td", textContent: item[key] }, tr);

        var img = document.createElement("img");
        img.setAttribute("src", "../images/edit.png");
        img.setAttribute("alt", "Nodepad Icon");

        img.id = `edit-${arrObj.indexOf(item)}`;

        var edTd = createTagElement({ tagName: "td", innerHTML: img.outerHTML }, tr)

        img.setAttribute("src", "../images/delete.png");
        img.setAttribute("alt", "Wastebin Icon");

        img.id = `delete-${arrObj.indexOf(item)}`;

        var edTd = createTagElement({ tagName: "td", innerHTML: img.outerHTML }, tr)
    }

}

/** 
 * ASC sorting
 * @author Samir Abo-Assfour
 */
function sortWithKeyAsc(array, keys) {
    return array.sort(function (a, b) {
        if (a[keys] > b[keys]) {
            return 1;
        }
        return -1;
    });
    let bar_status = document.querySelector("td#status-display");
    bar_status.textContent = `You have sorted the ${keys} in ascending order `;
}

/**
 * DESC sorting
 * @author Samir Abo-Assfour
 */
function sortWithKeyDesc(array, keys) {
    return array.sort(function (a, b) {
        if (a[keys] > b[keys]) {
            return -1;
        }
        return 1;
    });
    let bar_status = document.querySelector("td#status-display");
    bar_status.textContent = `You have sorted the ${keys} in descending order `;
}

function updateStatusDisplay(message) {
    let bar_status = document.querySelector("td#status-display");
    bar_status.textContent = message;

}

