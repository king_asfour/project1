/**
 * main_program.js - main logic for the webpage
 * @author Israel Aristide, Samir Abo-Assfour
 * @version 1.0.0
 */

"use strict";

// This event runs when the DOM is ready and all html elements are loaded
document.addEventListener("DOMContentLoaded", main);

/**
 * Global Variables
 */

var PROJECTS_TABLE_STORAGE = []
var FILTERED_TABLE_STORAGE = []

var FORM_INPUTS = []



let verification_status = {
    id: false, owner: false, title: false, category: false,
    hours: false, rate: false, status: false, description: false
};

let project_id_regex = /^[a-zA-z]{1}[a-zA-Z0-9_$-]{2,9}$/;
let project_owner_regex = /^[a-zA-z]{1}[a-zA-Z0-9-]{2,9}$/;
let project_title_regex = /[a-zA-z]{3,25}$/;
let project_hours_regex = /^\d[0-9]{0,3}$/;
let project_rate_regex = /^\d[0-9]{0,3}$/;
let project_description_regex = /^[a-zA-z0-9\s\d]{3,65}$/;

/**
 * Gets the global projects js variable.
 * @author Israel Aristide
 */
function getGlobalStorage() {
    return PROJECTS_TABLE_STORAGE;
}

/**
 * @author Israel Aristide
 * @param {*} newstorage 
 */
function setGlobalStorage(newstorage) {
    PROJECTS_TABLE_STORAGE = newstorage;
}

/**
 * @author Israel Aristide
 * @param {*} newstorage 
 */
function getFilteredStorage() {
    return FILTERED_TABLE_STORAGE;
}

/**
 * @author Israel Aristide
 * @param {*} newstorage 
 */
function setFilteredStorage(newstorage) {
    FILTERED_TABLE_STORAGE = newstorage;
}

/**
 * Main function used run when the dom content is loaded, used to initialize things
 * @param {*} event 
 * @author Israel Aristide, Samir Abo-Assfour
 */
function main(event) {
    
    FORM_INPUTS = [
        document.querySelector('input#project-id'), document.querySelector('input#project-owner'), document.querySelector('input#project-title'),
        document.querySelector('select#project-category'), document.querySelector('input#project-hours'), document.querySelector('input#project-rate'),
        document.querySelector('select#project-status'), document.querySelector('textarea#project-desc')
    ]

    // Use Query selector instead of getelmentbyid
    let id_input = FORM_INPUTS[0];
    let owner_input = FORM_INPUTS[1];
    let title_input = FORM_INPUTS[2];
    let category_input = FORM_INPUTS[3];
    let hours_input = FORM_INPUTS[4];
    let rate_input = FORM_INPUTS[5];
    let status_input = FORM_INPUTS[6];
    let description_input = FORM_INPUTS[7];

    let fnParams = [
        [id_input, 'Must be alphanumeric and and min 3 max 10 characters.', "id", project_id_regex],
        [owner_input,'Must be alphanumeric and and min 3 max 10 characters.', "owner", project_owner_regex],
        [title_input, 'must be between 3 and 25 characters.', "title", project_title_regex],
        [category_input, 'Please select a category.', "category",  category_input],
        [hours_input, 'Must be positive number and 3 or less digits.', "hours", project_hours_regex],
        [rate_input, 'must be positive number and 3 or less digits.', "rate", project_rate_regex],
        [status_input, 'Please select a status.', "status", status_input],
        [description_input, 'Must be 3 to 65 characters.', "description", project_description_regex]
    ]

    fnParams.forEach(params => {
        createBlurEvent(params[0], params[1], params[2], params[3]);
    });

    let btn = document.querySelector("button#add_btn");
    btn.addEventListener("click", () => {
        PROJECTS_TABLE_STORAGE.push(createProjectObject())
        updateProjectsTable(PROJECTS_TABLE_STORAGE);

        let bar_status = document.querySelector("td#status-display");
        bar_status.textContent = `Added a new row ..`;
    });

    let resetBtn = document.querySelector("button#reset_btn");
    resetBtn.addEventListener('click', function () {
        resetInputs();
    });

    let storageManage = document.querySelector('#storage-manager');

    storageManage.addEventListener('click', (event) => {
        
        switch (event.target.id) {
            case "save_btn":
                overwriteProjectsToStorage();
                break;
            case "append_btn":
                appendProjectsToStorage();
                break;
            case "clear_storage_btn":
                clearProjectsFromStorage()
                break;
            case "load_btn":
                readProjectsFromStorage();
                break;
            default:
                break;

        }

    });



    //----Event listener for sorting----
    let sortIcons = document.querySelectorAll("thead > tr > th > div");

    sortIcons.forEach(element => {
        
        element.addEventListener("click", (event) => {

            if (event.target.classList.contains("arrow-up")) {

                let storage = getGlobalStorage();
                let sortedTable = sortWithKeyAsc(storage, 'id');
                updateProjectsTable(sortedTable);
                element.classList.add('arrow-down');
                element.classList.remove('arrow-up')

            } else if (event.target.classList.contains("arrow-down")) {

                let storage = getGlobalStorage();
                let sortedTable = sortWithKeyDesc(storage, 'id');
                updateProjectsTable(sortedTable);
        
                element.classList.add('arrow-up');
                element.classList.remove('arrow-down');
            }
            
        })

    });

}

/**
 * sets the blur event for a given input object
 * @author Israel Aristide
 * @param {*} object The input object
 * @param {*} message The message when the input is invalid
 * @param {*} verify_type The string for indexing the verification_status object
 * @param {*} regex_string Regex to verify the input valie
 */
function createBlurEvent(object, message, verify_type, regex_string) {
    object.addEventListener('blur', function () {
        verification_status[verify_type] = validateElement(regex_string, object, message);
        setButtonDisabled("add_btn", !allValid(verification_status))
    })
}
