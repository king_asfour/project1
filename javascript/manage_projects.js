/**
 * manage_projects.js - Used to manage the table of projects.
 * @author Israel Aristide, Samir Abo-Assfour
 * @version 1.0.0
 */

"use strict";

document.addEventListener("DOMContentLoaded", main);

/**
 * Creates a project js object
 * @author Israel Aristide
 */
function createProjectObject() {

    let project_id = document.querySelector("input#project-id").value;
    let project_owner = document.querySelector("input#project-owner").value;
    let project_title = document.querySelector("input#project-title").value;
    let project_category = document.querySelector("select#project-category").value;
    let project_hours = document.querySelector("input#project-hours").value;
    let project_rate = document.querySelector("input#project-rate").value;
    let project_status = document.querySelector("select#project-status").value;
    let project_description = document.querySelector("textarea#project-desc").value;

    let project_object = {
        id: project_id, owner: project_owner, title: project_title, category: project_category,
        hours: project_hours, rate: project_rate, status: project_status,  description: project_description
    };

    return project_object;
}

/**
 * Event when the delete button is pressed on the table.
 * @author Israel Aristide
 * @param {*} event
 */
function deleteFromTable(event, index) {

    if (getFilteredStorage().length > 0) {
        alert("Please clear filters before deleting!");
        return;
    }

    let id_deleted = getGlobalStorage()[index].id;
    if (!confirm(`Are you sure you want to delete ${id_deleted} ?`))
        return;

    let storage = getGlobalStorage();

    storage.splice(parseInt(index), 1);

    updateProjectsTable(storage);

    let bar_status = document.querySelector("td#status-display");
    bar_status.textContent = `You have successfuly deleted: ${id_deleted} .. `;
}

/**
 * Event handler for image buttons inside of the table.
 * @author Israel Aristide
 * @param {*} event 
 */
function handleImageButton(event) {

    var identification = event.target.id.split('-');
    switch (identification[0]) {
        case 'delete':
            deleteFromTable(event, identification[1]);
            break;
        case 'edit':
            editTableRow(event, identification[1]);
            break;
        case 'save':
            saveTableRow(event, identification[1]);
        default:
            break;
    }

}

/**
 * Once the user fills out the form and it is correct, the current
 * project is visually shown in the table and added to the adequate storage.
 * @author Israel Aristide
 */
function updateProjectsTable(storage) {

    let tablebody = document.querySelector("#project-table > tbody");
    tablebody.replaceChildren();
    createTableContentFromArrayObjects(storage, tablebody);

    let imgbuttons = document.querySelectorAll("#project-table > tbody > tr > td img");

    imgbuttons.forEach(button => {
        button.addEventListener('click', handleImageButton);
    })

    
    setButtonDisabled('save_btn', (getGlobalStorage().length == 0)? true: false);
    

}

/**
 * When the icon (edit) next to the project is clicked, the whole row become
 * editable. The icon should turn to save icon, which when clicked update the project in
 * question.
 * @author Israel Aristide
 */
function editTableRow(event, index) {

    if (getFilteredStorage().length > 0) {
        alert("Please clear filters before editing!");
        return;
    }
    
    let tablerow = event.target.parentElement.parentElement;

    for (let i = 0; i < 8; i++) {
        let child = tablerow.children[i];

        let inp = FORM_INPUTS[i].cloneNode(true);
        inp.value = child.textContent;

        child.replaceChildren();
        child.appendChild(inp)
    }
    
    event.target.src = "../images/save.png"
    event.target.id = `save-${index}`;

}

/**
 * Run whenever the save button is clicked.
 * @author Israel Aristide, Samir Abo-Assfour
 * @param {*} event 
 * @param {*} index 
 */
function saveTableRow(event, index) {

    let tablerow = event.target.parentElement.parentElement;
    let storage = getGlobalStorage();

    for (let i = 0; i < 8; i++) {
        let key = Object.keys(storage[index])[i];
        let child = tablerow.children[i];
        let textcontent = child.children[0].value;

        child.replaceChildren();
        child.textContent = textcontent;
        storage[index][key] = textcontent;
    }

    event.target.src = "../images/edit.png"
    event.target.id = `edit-${index}`;
    updateStatusDisplay("Edits successfuly saved...")
}

/**
 * When the reset button is clicked, it should set all the inputs back to empty inputs 
 * @author Samir Abo-Assfour
 */
function resetInputs() {
    
    FORM_INPUTS.forEach(input => {

        input.value = null;

        if (input.options) 
            input.options["selectedIndex"] = 0

        input.dispatchEvent(new Event("blur"));
    })

    let bar_status = document.querySelector("td#status-display");
    bar_status.textContent = `You have reseted all of the input fields.. `;

}

/**
 * When the user clicks on button save to storage, all projects
 * in the table are saved to the browser local storage.
 * 
 * If the local storage contained some old projects, then they are overwritten.
 * Show an adequate message in the section status
 * 
 * If the table is empty, the button should be disabled.
 * @author Samir Abo-Assfour
*/
function overwriteProjectsToStorage() {
    let storage = getGlobalStorage();

    localStorage.setItem('storage', JSON.stringify(storage));

    let bar_status = document.querySelector("td#status-display");
    bar_status.textContent = `You have saved all the projects in the local storage.. `;
}

/**
 * Same as with saveAllProjects2Storage, except that the
 * new projects will be appended to a non-empty local storage
 * @author Samir Abo-Assfour
 */
function appendProjectsToStorage() {
    let originalStorage = JSON.parse(localStorage.getItem('storage')) || "[]";
    let newStorage = getGlobalStorage();
    let bar_status = document.querySelector("td#status-display");

    if (newStorage.length == 0) {
        bar_status.textContent = `Error! There is no value in the table `;
        return;
    }

    for (let item of newStorage) {
        originalStorage.push(item);
    }

    localStorage.setItem('storage', JSON.stringify(originalStorage));

    bar_status.textContent = `You added some projects to the local storage.. `;
}

/**
 * When the user clicks on the button "clear local", all
 * projects that are in local storage are removed
 * @author Samir Abo-Assfour
 */
function clearProjectsFromStorage() {
    localStorage.removeItem('storage');

    let bar_status = document.querySelector("td#status-display");
    bar_status.textContent = `You have cleared the local storage.. `;
}

/**
 * Read from the browser's local storage and visualize them
 * to the user.
 * @author Samir Abo-Assfour
 */

function readProjectsFromStorage() {
    let loadedLocStorage = JSON.parse(localStorage.getItem('storage'));
    if (loadedLocStorage == null) {
        let bar_status = document.querySelector("td#status-display");
        bar_status.textContent = `There is currently no projects in the local storage `;
        return;
    }

    setGlobalStorage(loadedLocStorage);
    updateProjectsTable(getGlobalStorage())

    let bar_status = document.querySelector("td#status-display");
    bar_status.textContent = `You loaded the local storage .. `;
}

/**
 * When the user enters any word(s) in the query input, those projects
 * whose field values match the keyword are returned and rendered.
 * @author Israel Aristide
 */
function filterProjects(filterstring) {

    let storage = getGlobalStorage();
    let filteredStorage = [];

    storage.forEach(project => {

        for (let key in project)
            if (project[key].toLowerCase().includes(filterstring.toLowerCase()))
                if (!filteredStorage.includes(project))
                    filteredStorage.push(project);

    });

    let regex = new RegExp(/^\s*$/);
    let verification = regex.test(filterstring);

    if (verification) {
        filteredStorage = storage;
        setFilteredStorage([]);
        updateProjectsTable(filteredStorage);
        updateStatusDisplay("");
        return;
    }

    setFilteredStorage(filteredStorage)
    updateProjectsTable(filteredStorage);
    updateStatusDisplay(`Found ${filteredStorage.length} projects with filter "${filterstring}" ..`);

}

/**
 * Main function used run when the dom content is loaded, used to initialize things
 * @param {*} event 
 * @author Israel Aristide, Samir Abo-Assfour
 */
function main(event) {

    let filterinput = document.querySelector("input#table-filter-field");

    filterinput.addEventListener('blur', (event) => {
        filterProjects(event.target.value);
    });

}