# Academic Project Management
    - Creation of projects that can be stored and save locally permanently.
## Description
### Purpose
    This application has as a goal to add projects that are set by the user. These project are set based on different
    values: project id, project owner, project title, etc. 
### What the project can do and its restrictions
    When the project are added to the table, there is a lot of options that we can apply on each one of them like editing the values of the project selected or delete the project. 
    However, to add project, the user should respectsome naming convention for each fields. If not respected, there is gonna be an error message telling the user what was the problem.
    A limitation that is important to say is that when the the user searches a project, the person
    can't edit nor delete the project. It should stop the filtering and then can delete or edit.
### What tools were used
    This project was typed with 3 different language: HTML, CSS and JavaScript.

## Installation/Run Project
    1- The first thing you need to do is to get the project and "download" it into your computer. To do that, use git clone on git bash by using the https.
    2- When the project is inside of the file you selected, open the html folder and open the html file. 
    And that's it! You can now use the website and add all of the projects you want with no problem
## Credits & contributions
    All images: Emojis from Apple inc.
## Liscence
    MIT

- Created by
Israel Aristide
Samir Abo-Assfour